'use strict';

window.$ = require('jquery')
window.jQuery = window.$
require('jquery-xml2json')
/**
 * 1
 */
console.log($.xml2json('<p>234234</p>'))

$(function() {

  var $textarea = $('textarea')
    , $jsonOut = $('.json')
  $jsonOut.text(JSON.stringify($.xml2json($textarea.val())))
  $textarea.bind('keyup change', function () {
    try {
      $jsonOut.text(JSON.stringify($.xml2json($textarea.val())))
    } catch(e) {
      $jsonOut.text('Ошибка не валидный XML')
    }
    
  })

})


/**
 * 2
 */
$(function() {

  var progression = []
    , $table = $('#table')
  
  for (var i = 1; i <= 100; i++)
    progression.push(i * 10 + (Math.floor(Math.random()*10))-5 )
  
  progression.forEach( (number) => $table.append('<span>' + number + '</span>') )

})

/**
 * 4
 */
var images = [
    {
        'name':'Изображение 1', 
        'file':'http://placebacon.net/800/1000?image=1'
    },
    {
        'name':'Изображение B', 
        'file':'http://placebacon.net/900/900?image=2'
    },
    {
        'name':'Изображение 4', 
        'file':'http://placebacon.net/1000/800?image=3'
    },
    {
        'name':'Изображение P', 
        'file':'http://placebacon.net/800/800?image=4'
    },
    {
        'name':'Изображение Q', 
        'file':'http://placebacon.net/900/900?image=5'
    }
]

$(function() {
  var $imageContainer = $('.thumbnail')
    , $imageTitle = $imageContainer.find('.thumbnail__title')
    , randomImage = images[Math.floor(Math.random()*(images.length-1))]
  
  $imageContainer.css('background-image', 'url(\''+ randomImage.file +'\')')
  $imageTitle.text(randomImage.name)
})

/**
 * 5
 */

$(function() {
  var $inputs = $('input')
    , $output = $('.output')

  $inputs.bind('keyup change', function () {
    var values = [parseFloat($inputs[0].value), parseFloat($inputs[1].value)]
    if (navigator.userAgent.match(/chrome/i))
      $output.text(Math.sqrt( Math.pow(values[0],2) + Math.pow(values[1],2) ))
    if (navigator.userAgent.match(/msie/i))
      $output.text(values[0]*values[1])
    if (navigator.userAgent.match(/firefox/i))
      $output.text(values[0]+values[1])
  })

})
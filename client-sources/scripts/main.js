'use strict';
/**
 * Dependencies
 */
window.jQuery = require('jquery')
window.app = require('angular').module('dmeTest', [])
require('./services.js')
var moment = require('moment')
require('../../node_modules/moment/locale/ru.js')

/**
 * News page controller
 * @param  {Object}   $scope     scope object
 * @param  {[Object]} categories Categories list
 * @param  {[Object]} news       News list
 * @param  {[Object]} events     Events list
 */
app.controller('dmeNewsController', ['$scope', 'dmeCategories', 'dmeNews', 'dmeEvents'
  , function ($scope, categories, news, events) {
  /**
   * Scope variables
   */
  [ $scope.categories
  , $scope.news
  , $scope.events
  , $scope.currentCategoryTitle ] = [ categories, news, events, categories[0].Title ]

  /**
   * Updates current category
   */
  $scope.setCurrentCategory = (title) => $scope.currentCategoryTitle = title

  /**
   * Resolves image url from post object
   */
  $scope.getImageForPost = function (post) {
    try {
      var src = post.PublishingRollupImage.match(/src=['"]((?:[^"'\/]*\/)*([^'"]+))['"]/)[1]
    } catch(e) {
      var src = 'нет фото'
    }
    return src
  }

  /**
   * Checks wether post's category is current
   */
  $scope.checkPostCategory = (post) =>
    post.PostCategory.some( (category) => 
      category.$2e_1 === $scope.currentCategoryTitle)

  /**
   * Extracts text from post
   */
  $scope.getTextForPost = function (post) {
    return jQuery(post.Body).find('p').contents().filter(function() {
      return this.nodeType === 3
    }).text()
  }

  /**
   * Formats date in common manner
   */
  $scope.formatDate = (date) => 
    moment(date).format('DD.MM.YYYY')

  /**
   * Builds post publising time string
   */
  $scope.getDateAndTimeForPost = (post) => 
    moment(post.PublishedDate).format('DD.MM.YYYY hh:mm')

  /**
   * Builds date's "from now" string
   */
  $scope.getEventDateFromNow = (event) => 
    moment(event.EventDate).fromNow()

}])
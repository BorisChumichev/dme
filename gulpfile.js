'use strict';

var gulp = require('gulp')
  , stylus = require('gulp-stylus')
  , browserify = require('browserify')
  , babelify = require('babelify')
  , source = require('vinyl-source-stream')
  , livereload = require('gulp-livereload')
  , jade = require('gulp-jade')
  , autoprefixer = require('gulp-autoprefixer')

gulp.task('stylus', function() {
  gulp.src('./client-sources/styles/main.styl')
    .pipe(stylus({
      'compress': true,
      'include css': true
    }))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./client/css'))
    .pipe(livereload())
})

gulp.task('scripts', function() {
  browserify({
    entries: './client-sources/scripts/main.js',
    debug: true,
    standalone: 'dme-test'
  })
  .transform(babelify)
  .bundle()
  .pipe(source('dme-test.js'))
  .pipe(gulp.dest('./client/js'))
  .pipe(livereload())
})

gulp.task('scripts-ex', function() {
  browserify({
    entries: './client-sources/scripts/exercises.js',
    debug: true,
    standalone: 'dme-exercises'
  })
  .transform(babelify)
  .bundle()
  .pipe(source('dme-exercises.js'))
  .pipe(gulp.dest('./client/js'))
  .pipe(livereload())
})
 
gulp.task('templates', function() { 
  gulp.src('./views/jade/*.jade')
    .pipe(jade())
    .pipe(gulp.dest('./views/html'))
    .pipe(livereload())
})

gulp.task('watch', function() {
  livereload.listen()
  gulp.watch(['./client-sources/**/**', './views/jade/*.jade'], ['stylus', 'scripts', 'templates', 'scripts-ex'])
})
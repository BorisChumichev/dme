# Структура проекта

        .
    ├── client <- скомпилированные исходники (CSS и JS)
    ├── client-sources <- исходники (CSS(Stylus) и JS)
    ├── gulpfile.js <- скрипт автоматизации (Gulp)
    ├── node_modules <- npm зависимости
    ├── package.json <- манифест
    └── views <- папка с разметой
        ├── html <- скомпилированный HTML
        └── jade <- исходники

Запускать views/html/mockup.html, views/html/exercises.html